===============
wagtailsettings
===============

``wagtailsettings`` has been merged in to Wagtail. `The Wagtail docs have more information <http://docs.wagtail.io/en/v1.6/reference/contrib/settings.html>`_. ``wagtailsettings`` is no longer maintained.
